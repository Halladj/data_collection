import React, { useState } from "react";
import { hot } from "react-hot-loader";
import { makeStyles } from "@material-ui/core/styles";
import { send } from "./api";
import {
  Card,
  CardContent,
  Typography,
  TextField,
  Icon,
  CardActions,
  Button,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  card: {
    maxWidth: 600,
    margin: "auto",
    textAlign: "center",
    marginTop: theme.spacing(5),
    paddingBottom: theme.spacing(2),
  },
  title: {
    margin: theme.spacing(2),
  },
  error: {
    verticalAlign: "middle",
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 300,
  },
  submit: {
    margin: "auto",
    marginBottom: theme.spacing(2),
  },
}));

const HelloWorld = () => {
  const classes = useStyles();
  const [values, setValues] = useState({
    brand: "",//done
    cpu_brand: "",//done
    cpu_modifier:"",//done
    cpu_number_identifier: 0,//done
    cpu_frequency: 0,//done
    cpu_family:"",
    gpu_brand: "",//done
    gpu_words_identifier: "",//done
    gpu_number_identifier: "",//done
    gpu_vram: 0,//done
    gpu_frequency: 0,//done
    state: "",//done
    screen_size: 0,//done
    touch_screen: 0,//done
    screen_resolution: "",//done
    screen_refresh_rate: 0,//done
    anti_glare: 0,//done
    ram: 0,//done
    ram_type: "",//done
    ram_frequency: 0,//done
    ssd: 0,
    hdd: 0,
    price: 0,
  });

  const clickSubmit = () => {
    const user = {
      brand: values.brand || undefined,
      cpu_brand: values.cpu_brand || undefined,
      cpu_modifier: values.cpu_modifier || undefined,
      cpu_number_identifier: values.cpu_number_identifier || undefined,
      cpu_frequency: values.cpu_frequency || undefined,
      cpu_family: values.cpu_family || undefined,
      gpu_brand: values.gpu_brand || undefined,
      gpu_words_identifier: values.gpu_words_identifier || undefined,
      gpu_number_identifier: values.gpu_number_identifier || undefined,
      gpu_vram: values.gpu_vram || undefined,
      gpu_frequency: values.gpu_frequency || undefined,
      state: values.state || undefined,
      screen_size: values.screen_size || undefined,
      touch_screen: values.touch_screen || undefined,
      screen_resolution: values.screen_resolution || undefined,
      screen_refresh_rate: values.screen_refresh_rate || undefined,
      anti_glare: values.anti_glare || undefined,
      ram: values.ram || undefined,
      ram_type: values.ram_type || undefined,
      ram_frequency: values.ram_frequency || undefined,
      ssd: values.ssd || undefined,
      hdd: values.hdd || undefined,
      price: values.price || undefined,
    };

    send(user);

    setValues({
      brand: "",
      cpu_brand: "",
      cpu_modifier:"",
      cpu_number_identifier: 0,
      cpu_family: "",
      cpu_frequency: 0,
      gpu_brand: "",
      gpu_words_identifier: "",
      gpu_number_identifier: "",
      gpu_vram: 0,
      gpu_frequency: 0,
      state: "",
      screen_size: 0,
      touch_screen: 0,
      screen_resolution: "",
      screen_refresh_rate: 0,
      anti_glare: 0,
      ram: 0,
      ram_type: "",
      ram_frequency: 0,
      ssd: 0,
      hdd: 0,
      price: 0,
    });
  };

  const handleChange = (name) => (event) => {
    setValues({ ...values, [name]: event.target.value });
  };

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography variant="h6" className={classes.title}>
          ADD NEW LAPTOP
        </Typography>
        <TextField
          id="brand"
          label="brand"
          className={classes.textField}
          value={values.brand}
          onChange={handleChange("brand")}
          margin="normal"
        />
        <br />
        <TextField
          id="gpu_brand"
          type="gpu_brand"
          label="gpu_brand"
          className={classes.textField}
          value={values.gpu_brand}
          onChange={handleChange("gpu_brand")}
          margin="normal"
        />
        <br />
        <TextField
          id="gpu_words_identifier"
          type="gpu_words_identifier"
          label="gpu_words_identifier"
          className={classes.textField}
          value={values.gpu_words_identifier}
          onChange={handleChange("gpu_words_identifier")}
          margin="normal"
        />
        <br />
        <TextField
          id="gpu_number_identifier"
          type="gpu_number_identifier"
          label="gpu_number_identifier"
          className={classes.textField}
          value={values.gpu_number_identifier}
          onChange={handleChange("gpu_number_identifier")}
          margin="normal"
        />
        <br />
        <TextField
          id="gpu_vram"
          type="gpu_vram"
          label="gpu_vram"
          className={classes.textField}
          value={values.gpu_vram}
          onChange={handleChange("gpu_vram")}
          margin="normal"
          type= "number"
        />
        <br />
        <TextField
          id="gpu_frequency"
          type="gpu_frequency"
          label="gpu_frequency"
          className={classes.textField}
          value={values.gpu_frequency}
          onChange={handleChange("gpu_frequency")}
          margin="normal"
          type= "number"
        />
        <br />
        <TextField
          id="cpu_brand"
          type="cpu_brand"
          label="cpu_brand"
          className={classes.textField}
          value={values.cpu_brand}
          onChange={handleChange("cpu_brand")}
          margin="normal"
        />
        <br />
        <TextField
          id="cpu_modifier"
          type="cpu_modifier"
          label="cpu_modifier"
          className={classes.textField}
          value={values.cpu_modifier}
          onChange={handleChange("cpu_modifier")}
          margin="normal"
        />
        <br />
        <TextField
          id="cpu_number_identifier"
          type="number"
          label="cpu_number_identifier"
          className={classes.textField}
          value={values.cpu_number_identifier}
          onChange={handleChange("cpu_number_identifier")}
          margin="normal"
        />
        <br />
        <TextField
          id="cpu_family"
          type="cpu_family"
          label="cpu_family"
          className={classes.textField}
          value={values.cpu_family}
          onChange={handleChange("cpu_family")}
          margin="normal"
        />
        <br />
        <TextField
          id="cpu_frequency"
          type="cpu_frequency"
          label="cpu_frequency"
          className={classes.textField}
          value={values.cpu_frequency}
          onChange={handleChange("cpu_frequency")}
          margin="normal"
          type= "number"
        />
        <br />
        <TextField
          id="state"
          type="state"
          label="state"
          className={classes.textField}
          value={values.state}
          onChange={handleChange("state")}
          margin="normal"
        />
        <br />
        <TextField
          id="screen_size"
          type="screen_size"
          label="screen_size"
          className={classes.textField}
          value={values.screen_size}
          onChange={handleChange("screen_size")}
          margin="normal"
          type= "number"
        />
        <br />
        <TextField
          id="touch_screen"
          type="touch_screen"
          label="touch_screen"
          className={classes.textField}
          value={values.touch_screen}
          onChange={handleChange("touch_screen")}
          margin="normal"
          type= "number"
        />
        <br />
        <TextField
          id="screen_resolution"
          type="screen_resolution"
          label="screen_resolution"
          className={classes.textField}
          value={values.screen_resolution}
          onChange={handleChange("screen_resolution")}
          margin="normal"
        />
        <br />
        <TextField
          id="screen_refresh_rate"
          label="screen_refresh_rate"
          className={classes.textField}
          value={values.screen_refresh_rate}
          onChange={handleChange("screen_refresh_rate")}
          margin="normal"
          type="number"
        />
        <br />
        <TextField
          id="anti_glare"
          type="anti_glare"
          label="anti_glare"
          className={classes.textField}
          value={values.anti_glare}
          onChange={handleChange("anti_glare")}
          margin="normal"
          type= "number"
        />
        <br />
        <TextField
          id="ram"
          label="ram"
          className={classes.textField}
          value={values.ram}
          onChange={handleChange("ram")}
          margin="normal"
          type="number"
        />
        <br />
        <TextField
          id="ram_type"
          label="ram_type"
          className={classes.textField}
          value={values.ram_type}
          onChange={handleChange("ram_type")}
          margin="normal"
        />
        <br />
        <TextField
          id="ram_frequency"
          label="ram_frequency"
          className={classes.textField}
          value={values.ram_frequency}
          onChange={handleChange("ram_frequency")}
          margin="normal"
          type="number"
        />
        <br />
        <TextField
          id="ssd"
          label="ssd"
          className={classes.textField}
          value={values.ssd}
          onChange={handleChange("ssd")}
          margin="normal"
          type="number"
        />
        <br />
        <TextField
          id="hdd"
          label="hdd"
          className={classes.textField}
          value={values.hdd}
          onChange={handleChange("hdd")}
          margin="normal"
          type="number"
        />
        <br />
        <TextField
          id="price"
          label="price"
          className={classes.textField}
          value={values.price}
          onChange={handleChange("price")}
          margin="normal"
          type="number"
        />
      </CardContent>
      <CardActions>
        <Button
          color="primary"
          variant="contained"
          onClick={clickSubmit}
          className={classes.submit}
        >
          Submit
        </Button>
      </CardActions>
    </Card>
  );
};

export default hot(module)(HelloWorld);
