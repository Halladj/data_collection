import Laptop from "./laptop.model";



const api_controller= async (req, res)=>{
  const labtpos= await Laptop.find({});
  return res.status(200).send(labtpos);
}

export {api_controller};
