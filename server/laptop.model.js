import mongoose from "mongoose";

const LaptopSchema = mongoose.Schema({
  brand: {
    type: String,
    required: "required"
  },
  cpu_brand: {
    type: String,
    required: "required"
  },
  cpu_modifier: {
    type: String,
    required: "required"
  },
  cpu_family:{
    type: String,
    required: "required"
  },
  cpu_number_identifier: {
    type: Number,
    required: "required"
  },
  cpu_frequency: {
    type: Number,
    required: "required",
    default: 0
  },
  gpu_brand: {
    type: String,
    required: "required",
    default: "0"
  },
  gpu_words_identifier: {
    type: String,
    required: "required",
    default: "0"
  },
  gpu_number_identifier: {
    type: String,
    required: "required",
    default: "0"
  },
  gpu_vram: {
    type: Number,
    required: "required",
    default: "0"
  },
  gpu_frequency: {
    type: Number,
    required: "required",
    default: "0"
  },
  state: {
    type: String,
    required: "required"
  },
  screen_size: {
    type: Number,
    required: "required"
  },
  touch_screen:{
    type: Number,
    required: "required",
    default: 0
  },
  screen_resolution: {
    type: String,
    required: "required"
  },
  screen_refresh_rate: {
    type: Number,
    required: "required",
    default: 60
  },
  anti_glare: {
    type: Number,
    required: "required",
    default: 0
  },
  ram: {
    type: Number,
    required: "required",
  },
  ram_type: {
    type: String,
    required: "required",
  },
  ram_frequency: {
    type: Number,
    required: "required",
    default: 0
  },
  ssd: {
    type: Number,
    required: "required",
    default: 0
  },
  hdd: {
    type: Number,
    required: "required",
    default: 0
  },
  price: {
    type: Number,
    required: "required"
  },
});

export default mongoose.model("Laptop", LaptopSchema);
